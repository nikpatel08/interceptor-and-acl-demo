package org.jahia.modules.custom.interceptor;

import org.apache.commons.lang.StringUtils;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.nodetypes.ExtendedPropertyDefinition;
import org.jahia.services.tags.TaggingService;

import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;
import org.jahia.services.content.interceptor.BaseInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PostPageInterceptor extends BaseInterceptor{
    private transient static Logger logger = LoggerFactory.getLogger(PostPageInterceptor.class);

    @Override
    public Value beforeSetValue(JCRNodeWrapper node, String name, ExtendedPropertyDefinition definition, Value originalValue) throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {

        logger.info("Name is : " + name);
        if (!node.hasPermission("updateOmniture")) {
            throw new RepositoryException("You do not have permission to save Omniture properties");
        }
        return originalValue;
    }


}
